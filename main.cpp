#include <QApplication>
#include <QLabel>
#include <QWSServer>
#include <QPushButton>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QWSServer::setBackground(QBrush(QColor(0, 0, 0, 0)));
    w.show();
    return a.exec();
}

