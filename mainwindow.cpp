#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include "loopback.h"

class MyThread : public QThread
{
    Q_OBJECT

protected:
    void run();
};

MyThread* loopback;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowFlags(Qt::FramelessWindowHint);

    loopback = new MyThread();

    if(init_loopback() < 0) {
        /* TODO: make sure exit occurs properly */
    }

    /* Configure GUI */
    this->setFixedSize(status.display_xres, status.display_yres);
    ui->frame->setGeometry(0, 0, status.display_xres, status.display_yres);
    ui->frame_pip->setGeometry(25, 25, status.display_xres/3, status.display_yres/3);
    ui->capture->setGeometry(status.display_xres/2 - 210,
                             status.display_yres - 50,
                             91, 41);
    ui->switch_2->setGeometry(status.display_xres/2 - 100,
                              status.display_yres - 50,
                              91, 41);
    ui->pip->setGeometry(status.display_xres/2 + 10,
                         status.display_yres - 50,
                         91, 41);
    ui->exit->setGeometry(status.display_xres/2 + 120,
                          status.display_yres - 50,
                          91, 41);

    if(status.num_cams == 1) {
        /* Reconfigure GUI to reflect single camera mode */
        ui->pip->setHidden(true);
        ui->pip->setDisabled(true);
        ui->switch_2->setHidden(true);
        ui->switch_2->setDisabled(true);
        ui->frame_pip->setHidden(true);
        ui->capture->setGeometry(status.display_xres/2 - 100,
                                 status.display_yres - 50,
                                 91, 41);
        ui->exit->setGeometry(status.display_xres/2 + 10,
                              status.display_yres - 50,
                              91, 41);
    }

    /* Start the camera loopback thread */
    loopback->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_capture_clicked()
{
    status.jpeg=true;
}

void MainWindow::on_switch_2_clicked()
{
    if (status.num_cams==1) status.main_cam=0;
    else if (status.main_cam==0) status.main_cam=1;
    else status.main_cam=0;
}

void MainWindow::on_pip_clicked()
{
    if (status.num_cams==1) {
        status.pip=false;
        ui->frame_pip->setHidden(true);
    }
    else if (status.pip==true) {
        ui->frame_pip->setHidden(true);
        status.pip=false;
        fbdev_disable_pip();
    }
    else {
        ui->frame_pip->setHidden(false);
        status.pip=true;
        fbdev_enable_pip();
    }
}

void MainWindow::on_exit_clicked()
{
    status.exit=true;
    loopback->wait(1000);
    this->close();
}

void MyThread::run() {

    while(status.exit == false) {
        /* Display captured frame and sleep to control FPS */
        process_frame();
        msleep(30);
    }
    end_streaming();
    exit_devices();
}


#include "mainwindow.moc"
