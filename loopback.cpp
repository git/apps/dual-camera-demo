#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <linux/fb.h>
#include <linux/omapfb.h>
#include <linux/videodev2.h>

#include "jpeg.h"
#include "loopback.h"

control status;

struct buf_info {
    unsigned int length;
    char *start;
};

/*
 * Fbdev output device structure declaration
 */
struct fbdev_device_info
{
    int fd;
    int width;
    int height;
    char dev_name[9];
    char name[4];
    int buf_current;
    char *buf_start;
    char *buf_start2;
    unsigned int buf_length;

    struct fb_fix_screeninfo fixinfo;
    struct fb_var_screeninfo varinfo;
    struct omapfb_display_info di;
    struct omapfb_mem_info mi;
    struct omapfb_plane_info pi;
    struct omapfb_caps caps;

    enum omapfb_update_mode update_mode;

} fb1_device, fb2_device;

/*
 * V4L2 capture device structure declaration
 */
struct v4l2_device_info {
    int type;
    int fd;
    enum v4l2_memory memory_mode;
    int num_buffers;
    int width;
    int height;
    char dev_name[12];
    char name[10];

    struct v4l2_buffer buf;
    struct v4l2_format fmt;
    struct buf_info *buffers;
} cap0_device, cap1_device;

static int v4l2_init_device(struct v4l2_device_info *device)
{
    int ret, i, j;
    struct v4l2_requestbuffers reqbuf;
    struct v4l2_buffer buf;
    struct v4l2_format temp_fmt;
    struct buf_info *temp_buffers;
    struct v4l2_capability capability;

    /* Open the capture device */
    device->fd = open((const char *) device->dev_name, O_RDWR);
    if (device->fd <= 0) {
        printf("Cannot open %s device\n", device->dev_name);
        return -1;
    }

    printf("\n%s: Opened Channel\n", device->name);

    /* Check if the device is capable of streaming */
    if (ioctl(device->fd, VIDIOC_QUERYCAP, &capability) < 0) {
        perror("VIDIOC_QUERYCAP");
        goto ERROR;
    }

    if (capability.capabilities & V4L2_CAP_STREAMING)
        printf("%s: Capable of streaming\n", device->name);
    else {
        printf("%s: Not capable of streaming\n", device->name);
        goto ERROR;
    }

    reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    temp_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    temp_fmt.fmt.pix.width = device->width;
    temp_fmt.fmt.pix.height = device->height;
    temp_fmt.fmt.pix.pixelformat = device->fmt.fmt.pix.pixelformat;

    ret = ioctl(device->fd, VIDIOC_S_FMT, &temp_fmt);
    if (ret < 0) {
        perror("VIDIOC_S_FMT");
        goto ERROR;
    }

    device->fmt = temp_fmt;

    reqbuf.count = device->num_buffers;
    reqbuf.memory = device->memory_mode;
    ret = ioctl(device->fd, VIDIOC_REQBUFS, &reqbuf);
    if (ret < 0) {
        perror("Cannot allocate memory");
        goto ERROR;
    }
    device->num_buffers = reqbuf.count;
    printf("%s: Number of requested buffers = %u\n", device->name,
           device->num_buffers);

    temp_buffers = (struct buf_info *) malloc(sizeof(struct buf_info) *
                          device->num_buffers);
    if (!temp_buffers) {
        printf("Cannot allocate memory\n");
        goto ERROR;
    }

    for (i = 0; i < device->num_buffers; i++) {
        buf.type = reqbuf.type;
        buf.index = i;
        buf.memory = reqbuf.memory;
        ret = ioctl(device->fd, VIDIOC_QUERYBUF, &buf);
        if (ret < 0) {
            perror("VIDIOC_QUERYCAP");
            device->num_buffers = i;
            goto ERROR1;
            return -1;
        }

        temp_buffers[i].length = buf.length;
        temp_buffers[i].start = (char*)mmap(NULL, buf.length,
                     PROT_READ | PROT_WRITE,
                     MAP_SHARED, device->fd,
                     buf.m.offset);
        if (temp_buffers[i].start == MAP_FAILED) {
            printf("Cannot mmap = %d buffer\n", i);
            device->num_buffers = i;
            goto ERROR1;
        }
        printf("temp_buffers[%d].start - %x\n", i,
                (unsigned int)temp_buffers[i].start);
    }

    device->buffers = temp_buffers;

    printf("%s: Init done successfully\n", device->name);
    return 0;

ERROR1:
    for (j = 0; j < device->num_buffers; j++)
        munmap(temp_buffers[j].start,
               temp_buffers[j].length);

    free(temp_buffers);
ERROR:
    close(device->fd);

    return -1;
}

static void v4l2_exit_device(struct v4l2_device_info *device)
{
    int i;

    for (i = 0; i < device->num_buffers; i++) {
        munmap(device->buffers[i].start,
               device->buffers[i].length);
    }

    free(device->buffers);
    close(device->fd);

    return;
}


/*
 * Enable streaming for V4L2 capture device
 */
static int v4l2_stream_on(struct v4l2_device_info *device)
{
    int a, i, ret;

    for (i = 0; i < device->num_buffers; ++i) {
            struct v4l2_buffer buf;

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = i;

            ret = ioctl(device->fd, VIDIOC_QBUF, &buf);
            if (ret < 0) {
                perror("VIDIOC_QBUF");
                device->num_buffers = i;
                return -1;
            }
    }

    device->buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    device->buf.index = 0;
    device->buf.memory = device->memory_mode;

    a = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ret = ioctl(device->fd, VIDIOC_STREAMON, &a);
    if (ret < 0) {
        perror("VIDIOC_STREAMON");
        return -1;
    }
    printf("%s: Stream on\n", device->name);

    return 0;
}

/*
 * Disable streaming for V4L2 capture device
 */
static int v4l2_stream_off(struct v4l2_device_info *device)
{
    int a, ret;

    a = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ret = ioctl(device->fd, VIDIOC_STREAMOFF, &a);
    if (ret < 0) {
        perror("VIDIOC_STREAMOFF");
        return -1;
    }
    printf("%s: Stream off\n", device->name);

    return 0;
}

/*
 * Queue V4L2 buffer
 */
static int v4l2_queue_buffer(struct v4l2_device_info *device)
{
    int ret;
    fd_set fds;
    struct timeval tv;

    FD_ZERO(&fds);
    FD_SET(device->fd, &fds);

    /* Timeout. */
    tv.tv_sec = 10;
    tv.tv_usec = 0;

    ret = select(device->fd + 1, &fds, NULL, NULL, &tv);

    if (-1 == ret) {
        if (EINTR == errno) {
            perror( "select\n");
            return -1;
        }
    }

    if (0 == ret) {
        perror( "select timeout\n");
        return -1;
    }

    /* Queue buffer for the v4l2 capture device */
    ret = ioctl(device->fd, VIDIOC_QBUF,
            &device->buf);
    if (ret < 0) {
        perror("VIDIOC_QBUF");
        return -1;
    }

    return 0;
}

/*
 * DeQueue V4L2 buffer
 */
static int v4l2_dequeue_buffer(struct v4l2_device_info *device)
{
    int ret;
    fd_set fds;
    struct timeval tv;

    FD_ZERO(&fds);
    FD_SET(device->fd, &fds);

    /* Timeout. */
    tv.tv_sec = 10;
    tv.tv_usec = 0;

    ret = select(device->fd + 1, &fds, NULL, NULL, &tv);

    if (-1 == ret) {
        if (EINTR == errno) {
            perror( "select\n");
            return -1;
        }
    }

    if (0 == ret) {
        perror( "select timeout\n");
        return -1;
    }

    /* Dequeue buffer for the v4l2 capture device */
    ret = ioctl(device->fd, VIDIOC_DQBUF,
            &device->buf);
    if (ret < 0) {
        perror("VIDIOC_DQBUF");
        return -1;
    }

    return 0;
}

/*
 * Fbdev device init
 */
static int fbdev_init_device(struct fbdev_device_info *device)
{
    /* Open the display device */
    if ((device->fd = open(device->dev_name, O_RDWR)) <= 0) {
        printf("Cannot open %s.\n", device->name);
        exit(0);
    }

    /* Get fix screen information */
    if (ioctl(device->fd, FBIOGET_FSCREENINFO, &device->fixinfo) < 0) {
        printf("Error reading fixed information for: %s\n", device->name);
        exit(0);
    }

    /* Get variable screen information */
    if (ioctl(device->fd, FBIOGET_VSCREENINFO, &device->varinfo) < 0) {
        printf("Error reading variable information for: %s\n", device->name);
        exit(0);
    }

    if (ioctl(device->fd, OMAPFB_GET_DISPLAY_INFO, &device->di)) {
        printf("OMAPFB_GET_DISPLAY_INFO error for: %s\n", device->name);
        exit(0);
    }

    /* Now the memory for the fb device needs to be set up */

    if (ioctl(device->fd, OMAPFB_QUERY_PLANE, &device->pi)) {
        printf("OMAPFB_QUERY_PLANE error for: %s\n", device->name);
        exit(0);
    }

    if (device->pi.enabled) {
        device->pi.enabled = 0;
        if (ioctl(device->fd, OMAPFB_SETUP_PLANE, &device->pi)) {
            printf("OMAPFB_SETUP_PLANE error for: %s\n", device->name);
            exit(0);
        }
    }

    printf("\nDisplay Screen Properties:\n");
    printf("%s bpp: %d\n",device->name, device->varinfo.bits_per_pixel);
    printf("%s di resolution: %dx%d\n",device->name,
           device->di.xres, device->di.yres);

    /* Store display resolution so GUI can be configured */
    status.display_xres = device->di.xres;
    status.display_yres = device->di.yres;

    if (device->di.xres < 640) {
        device->width = 640;
        device->height = 480;
    }

    /* Set the input resolution of the video framebuffer to be the size
     * of the camera image.  Later, the output size will be matched to
     * the display to allow for up/down scaling */
    device->varinfo.xres = device->width;
    device->varinfo.yres = device->height;
    device->varinfo.xres_virtual = device->width;
    device->varinfo.yres_virtual = 2 * device->height;

    device->varinfo.bits_per_pixel = 16;
    device->varinfo.nonstd = OMAPFB_COLOR_YUY422;

    if (ioctl(device->fd, OMAPFB_QUERY_MEM, &device->mi)) {
        printf("OMAPFB_QUERY_PLANE error for: %s\n", device->name);
        exit(0);
    }

    device->mi.size = device->varinfo.xres_virtual * device->varinfo.yres_virtual *
            device->varinfo.bits_per_pixel / 8;

    if (ioctl(device->fd, OMAPFB_SETUP_MEM, &device->mi)) {
        printf("OMAPFB_QUERY_PLANE error for: %s\n", device->name);
        exit(0);
    }

    if (ioctl(device->fd, FBIOPUT_VSCREENINFO, &device->varinfo) < 0) {
        printf("Error setting variable information for: %s\n", device->name);
        exit(0);
    }

    device->pi.pos_x = 0;
    device->pi.pos_y = 0;
    /* Set the the display plane output size to the detected resolution
     * of the lcd/monitor.  This will cause the 800x600 camera image to
     * be up/downscaled to fit the screen */
    device->pi.out_width = device->di.xres;
    device->pi.out_height = device->di.yres;
    device->pi.enabled = 1;

    /* Get device capabilities */
    ioctl(device->fd,OMAPFB_GET_CAPS, &device->caps);

    printf("Checking for manual update capability...\n");
    if (device->caps.ctrl & OMAPFB_CAPS_MANUAL_UPDATE)
            printf("Manual update supported\n");

    ioctl(device->fd,OMAPFB_GET_UPDATE_MODE, &device->update_mode);
    printf("OMAPFB_GET_UPDATE_MODE: %d\n", device->update_mode);

    if (ioctl(device->fd, OMAPFB_SETUP_PLANE, &device->pi)) {
        printf("OMAPFB_SETUP_PLANE error for: %s\n", device->name);
        exit(0);
    }

    /* Get fix screen information */
    if (ioctl(device->fd, FBIOGET_FSCREENINFO, &device->fixinfo) < 0) {
        printf("Error reading fixed information for: %s\n", device->name);
        exit(0);
    }

    /* Get variable screen information */
    if (ioctl(device->fd, FBIOGET_VSCREENINFO, &device->varinfo) < 0) {
        printf("Error reading variable information for: %s\n", device->name);
        exit(0);
    }

    printf("Camera Display Overlay Properties:\n");
    printf("resolution %dx%d virtual %dx%d, line_len %d\n",
                device->varinfo.xres, device->varinfo.yres,
                device->varinfo.xres_virtual, device->varinfo.yres_virtual,
                device->fixinfo.line_length);

    /* Mmap the driver buffers in application space so that application
     * can write on to them.  Buffers need to be 2x for double buffering
     * to eliminate tearing
     */
    device->buf_length = device->fixinfo.line_length * device->varinfo.yres_virtual;
    device->buf_start = (char *)mmap (0, device->buf_length,
            (PROT_READ|PROT_WRITE), MAP_SHARED, device->fd, 0);
    device->buf_start2 = device->buf_start + device->buf_length/2;

    printf("1st buffer addr: %x\n", (unsigned int)device->buf_start);
    printf("2nd buffer addr: %x\n", (unsigned int)device->buf_start2);

    if (device->buf_start == MAP_FAILED) {
        printf("MMap failed for %s\n", device->name);
        exit(0);
    }

    return 0;
}

static void fbdev_exit_device(struct fbdev_device_info *device)
{
    /* Clear framebuffer of data */
    memset(device->buf_start, 0, device->buf_length);

    device->pi.enabled = 0;
    if (ioctl(device->fd, OMAPFB_SETUP_PLANE, &device->pi)) {
        printf("OMAPFB_SETUP_PLANE error for: %s\n", device->name);
    }

    munmap(device->buf_start, device->buf_length);
    close(device->fd);

    return;
}

/*
 * Set up the DSS for blending of video and graphics planes
 */
static int fbdev_init_dss(void)
{
    FILE *f;

    /* Set manager for alphablending */
    f = fopen("/sys/devices/platform/omapdss/manager0/alpha_blending_enabled", "w");
    fprintf(f,"%d", 1);
    fclose(f);

    f = fopen("/sys/devices/platform/omapdss/manager0/trans_key_enabled", "w");
    fprintf(f,"%d", 1);
    fclose(f);

    /* Set scalar for PiP video plane to 1/3 of main video plane */
    fb2_device.pi.enabled = 0;
    fb2_device.pi.out_width = fb1_device.pi.out_width/3;
    fb2_device.pi.out_height = fb1_device.pi.out_height/3;
    fb2_device.pi.pos_x = 25;
    fb2_device.pi.pos_y = 25;
    fb2_device.pi.enabled = 1;

    if (ioctl(fb2_device.fd, OMAPFB_SETUP_PLANE, &fb2_device.pi)) {
        printf("OMAPFB_SETUP_PLANE error for: %s\n", fb2_device.name);
        return -1;
    }

    return 0;
}

/*
 * Fbdev enable pip layer
 */
int fbdev_enable_pip(void)
{
    fb2_device.pi.enabled = 1;
    if (ioctl(fb2_device.fd, OMAPFB_SETUP_PLANE, &fb2_device.pi)) {
        printf("OMAPFB_SETUP_PLANE error for: %s\n", fb2_device.name);
        return -1;
    }

    return 0;
}

/*
 * Fbdev disable pip layer
 */
int fbdev_disable_pip(void)
{
    fb2_device.pi.enabled = 0;
    if (ioctl(fb2_device.fd, OMAPFB_SETUP_PLANE, &fb2_device.pi)) {
        printf("OMAPFB_SETUP_PLANE error for: %s\n", fb2_device.name);
        return -1;
    }

    return 0;
}

/*
 * Display v4l2 frame on fbdev device
 */
static int display_frame(struct v4l2_device_info *v4l2_device, struct fbdev_device_info *fbdev_device)
{
    int fb_size = fbdev_device->fixinfo.line_length*fbdev_device->varinfo.yres;

    /* Request a capture buffer from the driver that can be copied to framebuffer */
    v4l2_dequeue_buffer(v4l2_device);

    /* Copy the new camera frame into the buffer that is not currently displayed */
    if (fbdev_device->buf_current == 0)
    {
        /* Copy the contents of the v4l2 capture buffer to framebuffer */
        memcpy(fbdev_device->buf_start2,
        v4l2_device->buffers[v4l2_device->buf.index].start,
        fb_size);

        /* After the buffer copy is complete, tell the DSS to display the new data */
        fbdev_device->varinfo.yoffset = 600;
        ioctl(fbdev_device->fd,FBIOPAN_DISPLAY, fbdev_device->varinfo);
        if (ioctl(fbdev_device->fd, FBIOPUT_VSCREENINFO, &fbdev_device->varinfo) < 0) {
            printf("Error setting variable information for: %s\n", fbdev_device->name);
            exit(0);
        }
        fbdev_device->buf_current=1;
    }
    else {
        /* Copy the contents of the v4l2 capture buffer to framebuffer */
        memcpy(fbdev_device->buf_start,
        v4l2_device->buffers[v4l2_device->buf.index].start,
        fb_size);

        /* After the buffer copy is complete, tell the DSS to display the new data */
        fbdev_device->varinfo.yoffset = 0;
        ioctl(fbdev_device->fd,FBIOPAN_DISPLAY, fbdev_device->varinfo);
        if (ioctl(fbdev_device->fd, FBIOPUT_VSCREENINFO, &fbdev_device->varinfo) < 0) {
            printf("Error setting variable information for: %s\n", fbdev_device->name);
            exit(0);
        }
        fbdev_device->buf_current=0;
    }


    /* Give the buffer back to the driver so it can be filled again */
    v4l2_queue_buffer(v4l2_device);

    return 0;
}

/*
 * Capture v4l2 frame and save to jpeg
 */
static int capture_frame(struct v4l2_device_info *v4l2_device)
{
    /* Request a capture buffer from the driver that can be copied to framebuffer */
    v4l2_dequeue_buffer(v4l2_device);

    jpegWrite((unsigned char *)v4l2_device->buffers[cap0_device.buf.index].start,
            status.num_jpeg, v4l2_device->width, v4l2_device->height);

    /* Give the buffer back to the driver so it can be filled again */
    v4l2_queue_buffer(v4l2_device);

    return 0;
}

void default_parameters(void)
{
    /* Main camera display */
    strcpy(fb1_device.dev_name,"/dev/fb1");
    strcpy(fb1_device.name,"fb1");
    fb1_device.width=800;
    fb1_device.height=600;
    fb1_device.buf_current=0;

    /* PiP camera display */
    strcpy(fb2_device.dev_name,"/dev/fb2");
    strcpy(fb2_device.name,"fb2");
    fb2_device.width=800;
    fb2_device.height=600;
    fb2_device.buf_current=0;

    /* Main camera */
    cap0_device.memory_mode = V4L2_MEMORY_MMAP;
    cap0_device.num_buffers = 3;
    strcpy(cap0_device.dev_name,"/dev/video0");
    strcpy(cap0_device.name,"Capture 0");
    cap0_device.buffers = NULL;
    cap0_device.fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    cap0_device.width = 800;
    cap0_device.height = 600;

    /* PiP camera */
    cap1_device.memory_mode = V4L2_MEMORY_MMAP;
    cap1_device.num_buffers = 3;
    strcpy(cap1_device.dev_name,"/dev/video1");
    strcpy(cap1_device.name,"Capture 1");
    cap1_device.buffers = NULL;
    cap1_device.fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    cap1_device.width = 800;
    cap1_device.height = 600;

    /* Set the default parameters for device options */
    status.main_cam=0;
    status.num_cams=2;
    status.num_jpeg=0;
    status.pip=true;
    status.jpeg=false;
    status.exit=false;

    /* Ensure that jpeg image save directory exists */
    mkdir("/usr/share/camera-images/", 0777);

    return;
}

void exit_devices(void)
{
    fbdev_exit_device(&fb1_device);
    fbdev_exit_device(&fb2_device);

    v4l2_exit_device(&cap0_device);
    if (status.num_cams==2) {
        v4l2_exit_device(&cap1_device);
    }
}

void end_streaming(void)
{
    v4l2_stream_off(&cap0_device);

    if (status.num_cams==2) {
       v4l2_stream_off(&cap1_device);
    }
}

/*
 * Initializes all fbdev and v4l2 devices for loopback
 */
int init_loopback(void)
{
    /* Declare properties for video and capture devices */
    default_parameters();

    /* Initialize the fbdev display devices */
    if (fbdev_init_device(&fb1_device)) goto Error;
    if (fbdev_init_device(&fb2_device)) goto Error;

    /* Check to see if the display resolution is very small.  If so, the
     * camera capture resolution needs to be lowered so that the scaling
     * limits of the DSS are not reached */
    if (fb1_device.di.xres < 640) {
        /* Set capture 0 device resolution */
        cap0_device.width = 640;
        cap0_device.height = 480;

        /* Set capture 1 device resolution */
        cap1_device.width = 640;
        cap1_device.height = 480;
    }

    /* Initialize the v4l2 capture devices */
    if (v4l2_init_device(&cap0_device) < 0) goto Error;
    if (v4l2_init_device(&cap1_device) < 0) {
        /* If there is not a second camera, program can still continue */
        status.num_cams=1;
        status.pip=false;
        printf("Only one camera detected\n");
    }

    /* Enable streaming for the v4l2 capture devices */
    if (v4l2_stream_on(&cap0_device) < 0) goto Error;
    if (status.num_cams==2) {
        if (v4l2_stream_on(&cap1_device) < 0) goto Error;
    }

    /* Configure the DSS to blend video and graphics layers */
    if (fbdev_init_dss() < 0 ) goto Error;

    if (status.pip==false)
        fbdev_disable_pip();
    else
        fbdev_enable_pip();

    return 0;

Error:
    exit_devices();
    status.exit = true;
    return -1;
}

/*
 * Determines which camera feeds are being displayed and
 * whether a jpeg image needs to be captured.
 */
void process_frame(void) {

    /* Display the main camera */
    if (status.main_cam==0)
        display_frame(&cap0_device, &fb1_device);
    else
        display_frame(&cap1_device, &fb1_device);

    /* Display PiP if enabled */
    if (status.pip==true) {
        if (status.main_cam==0)
            display_frame(&cap1_device, &fb2_device);
        else
            display_frame(&cap0_device, &fb2_device);
    }

    /* Save jpeg image if triggered */
    if (status.jpeg==true) {
        if (status.main_cam==0) {
            capture_frame(&cap0_device);
        }
        else {
            capture_frame(&cap1_device);
        }

        status.jpeg=false;
        status.num_jpeg++;
        if (status.num_jpeg==10)
            status.num_jpeg=0;
    }
}
