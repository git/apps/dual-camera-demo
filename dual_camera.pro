#-------------------------------------------------
#
# Project created by QtCreator 2014-01-15T08:40:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets threads

TARGET = dual_camera
TEMPLATE = app
LIBS += -ljpeg

SOURCES += main.cpp\
        mainwindow.cpp \
    jpeg.cpp \
    loopback.cpp

HEADERS  += mainwindow.h \
    jpeg.h \
    loopback.h

FORMS    += mainwindow.ui
