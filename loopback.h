#ifndef LOOPBACK_H
#define LOOPBACK_H

struct control {
    int main_cam;
    int num_cams;
    int num_jpeg;
    int display_xres, display_yres;
    bool pip;
    bool jpeg;
    bool exit;
};

extern control status;

int init_loopback(void);
void process_frame(void);
void end_streaming(void);
void exit_devices(void);
int fbdev_enable_pip(void);
int fbdev_disable_pip(void);

#endif // LOOPBACK_H
